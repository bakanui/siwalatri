import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CRow,
  CModal, 
  CModalHeader, 
  CModalTitle, 
  CModalBody, 
  CModalFooter, 
  CWidgetBrand,
  CCol
} from '@coreui/react'
import {
  CChartBar,
  CChartLine,
  CChartDoughnut,
  CChartRadar,
  CChartPie,
  CChartPolarArea
} from '@coreui/react-chartjs'
import CIcon from '@coreui/icons-react'
import useToken from '../../../src/useToken';
import { apiUrl } from './../../reusable/constants'
import Penumpangs from './../../components/Penumpangs'
import { MapLocation } from 'src/reusable/MapLocation';
import warningIcon from '../../assets/img/warning.png';
import { ref, onValue } from "firebase/database";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAnchor, faShip, faUser } from '@fortawesome/free-solid-svg-icons'
import database from 'src/firebase_init';
import { Link } from 'react-router-dom';
const Dashboard = () => {
  const postArmada = [
    {
      "nama_armada": "Gangga Express",
      "total": 0,
    }
  ]

  const { token,type,id,id_armada } = useToken();
  const [armadas, setArmada] = useState({data : postArmada});
  const [jadwals, setJadwals] = useState({data : []});
  const [jadwalnya, setJadwalnya] = useState([]);
  const [kapal, setKapals] = useState({data : []});
  const [showEmergencyModal, setShowEmergencyModal] = useState(false);
  const [emergencyData, setEmergencyData] = useState([]);
  const [emergencyDataActive, setEmergencyDataActive] = useState(null);
  const [idKeberangkatan, setIdKeberangkatan] = useState(null);
  const headers = {
    headers: {
      'Authorization': "bearer " + token 
    },
    timeout: 10000 
  }

  useEffect(() => {
    fetchData();
    _listenEmergencyNotification();
    // eslint-disable-next-line
  }, [])

  const getBadge = (status)=>{
    switch (status) {
      case 'Berlayar': return 'success'
      case 'Sandar': return 'secondary'
      case 'Persiapan': return 'warning'
      default: return 'primary'
    }
  }

  const getBadgeStatus = (status)=>{
    switch (status) {
      case 'Active': return 'success'
      case 'Inactive': return 'danger'
      default: return 'info'
    }
  }


  const fetchData = async () => {
    if(type === 'admin' || type === 'syahbandar'){
      const result = await axios.get(apiUrl + 'jadwal_keberangkatan/get-total', headers)
      .catch(function (error) {
        if(error.response?.status === 401){
            localStorage.removeItem('access_token')
            window.location.reload()
        }
      })
      setArmada(result)
      const jadwals = await axios.get(apiUrl + 'jadwal_keberangkatan/get-dashboard', headers)
      setJadwals(jadwals)
    }else if(type === 'armada'){
      const result = await axios.get(apiUrl + 'kapal/'+id, headers)
      .catch(function (error) {
        if(error.response?.status === 401){
            localStorage.removeItem('access_token')
            window.location.reload()
        }
      })
      setKapals(result)
      // console.log(result)
      const jad = await axios.get(apiUrl + 'jadwal_keberangkatan/index/'+id, headers)
      setJadwalnya(jad.data.jadwal)
      // console.log(jad.data.jadwal);
    }else if(type === 'loket'){
      const jad = await axios.get(apiUrl + 'jadwal_keberangkatan/index/'+id_armada, headers)
      setJadwalnya(jad.data.jadwal)
    }
  }

  function getCardArmadas(datas){
    const listItems = 
        datas.map((data, i) =>
          {
            return(
              <CCol sm="12" lg="4">
                <CWidgetBrand
                  color="facebook"
                  rightHeader={data.total_trip}
                  rightFooter="trip"
                  leftHeader={data.total_penumpang}
                  leftFooter="penumpang"
                >
                  <CIcon
                    name="cil-boat-alt"
                    height="56"
                    className="my-4"
                  />
                  {data.nama_armada.includes("Angkal") ? <p className="h4">&nbsp;&nbsp;{data.nama_armada}</p> : <p className="h3">&nbsp;&nbsp;{data.nama_armada}</p>}
                </CWidgetBrand>
              </CCol>
            )
          }
          
      );
      return listItems;
  }

  function getPieArmadas(datas){
    let labels = []
    let data = []
    datas.map((d) => {
      labels.push(d.nama_armada)
      data.push(d.total_penumpang)
    })
    return(
      <CChartPie
        datasets={[
          {
            backgroundColor: [
              '#41B883',
              '#E46651',
              '#00D8FF',
              '#DD1B16'
            ],
            data: data
          }
        ]}
        labels={labels}
        options={{
          tooltips: {
            enabled: true
          }
        }}
      />
    )
  }

  function getCardKapal(datas){
      return(
          <div className='grey-thead'>
            <CDataTable
                  items={datas}
                  fields={[
                    { key: 'nama_kapal', _style: { width: '15%'}},
                    { key: 'mesin', _style: { width: '10%'} },
                    { key: 'kapal_jenis', _style: { width: '10%'} },
                    { key: 'kapasitas_penumpang', _style: { width: '10%'} },
                    { key: 'status', _style: { width: '10%'} },
                  ]}
                  columnFilter
                  // tableFilter
                  button
                  hover
                  pagination
                  bordered
                  striped
                  size="sm"
                  itemsPerPage={5}
                  scopedSlots = {{
                      'nama_kapal': 
                        (item,index)=> (
                          <td key={index}>
                            {item.nama_kapal}
                          </td>
                        ),
                        'mesin':
                        (item)=>(
                          <td>
                            {item.mesin}
                          </td>
                        ),
                        'kapal_jenis': 
                        (item)=> (
                          <td>
                            {item.kapal_to_jenis.nama_jenis}
                          </td>
                        ),
                        'kapasitas_penumpang': 
                        (item)=> (
                          <td>
                            {item.kapasitas_penumpang}
                          </td>
                        ),
                        'status': 
                        (item)=> (
                          <td>
                            <CBadge color={getBadgeStatus(item.kapal_to_status.nama_status)}>
                              {item.kapal_to_status.nama_status}
                            </CBadge>
                          </td>
                        ),
                  }}
              />
          </div>
        )
  }

  const _listenEmergencyNotification = () => {
    onValue(ref(database, '/emergency/'), (snapshot) => {
      let dataTmp = [];
      snapshot.forEach((child)=>{
        dataTmp.push({
          id: child.key,
          armada: child.val().armada,
          date: child.val().date,
          id_armada: child.val().id_armada,
          nama_kapal: child.val().kapal,
          kode: child.val().kode,
          nahkoda: child.val().nahkoda,
        });
        setEmergencyData(dataTmp);
      });
    });
  }

  const _fokusShipLocation = () => {
    setShowEmergencyModal(false);
    setIdKeberangkatan(emergencyDataActive ? emergencyDataActive.id : null);
  }

  return (
    <>
    {(() => {
      if(type === 'admin' || type === 'syahbandar'){
        return(
          <div className='conteiner-operator'>
            {/* <CCard className='p-3'>
              <MapLocation id={null} id_keberangkatan={idKeberangkatan}/>
            </CCard> */}
            <CRow>
              {armadas?.data ? getCardArmadas(armadas.data) : ''}
            </CRow>
            <CRow>
              <div className='col-lg-12 col-xs-12 col-sm-12 col-md-12'>
                <CCard>
                  <CCardHeader>
                   Grafik Armada
                  </CCardHeader>
                  <CCardBody>
                    {armadas?.data ? getPieArmadas(armadas.data) : ''}
                  </CCardBody>
                </CCard>
              </div>
            </CRow>
            <CRow>
                <div className='col-lg-12 col-xs-12 col-sm-12 col-md-12'>
                <CCard>
                  <CCardHeader>
                  Daftar Keberangkatan
                  </CCardHeader>
                    <CDataTable
                      items={jadwals.data}
                      fields={[
                        { key: 'nama_armada', _style: { width: '15%'}},
                        { key: 'jenis_jadwal', _style: { width: '10%'} },
                        { key: 'jadwal', _style: { width: '10%'} },
                        { key: 'nama_kapal', _style: { width: '10%'} },
                        { key: 'tujuan_awal', _style: { width: '20%'} },
                        { key: 'tujuan_akhir', _style: { width: '10%'} },
                        { key: 'status', _style: { width: '10%'} },
                      ]}
                      columnFilter
                      button
                      hover
                      pagination
                      bordered
                      striped
                      size="sm"
                      itemsPerPage={5}
                      scopedSlots = {{
                          'nama_armada':
                            (item, index)=>(
                              <td key={index}>
                                {item.nama_armada}
                              </td>
                            ),
                            'jadwal':
                            (item)=>(
                              <td>
                                {item.jadwal}
                              </td>
                            ),
                            'nama_kapal': 
                            (item)=> (
                              <td>
                                {item.nama_kapal}
                              </td>
                            ),
                            'tujuan_awal': 
                            (item)=> (
                              <td>
                                {item.tujuan_awal} - {item.lokasi_awal}
                              </td>
                            ),
                            'tujuan_akhir': 
                            (item)=> (
                              <td>
                                {item.tujuan_akhir} - {item.lokasi_akhir}
                              </td>
                            ),
                            'status': 
                            (item)=> (
                              <td>
                                <CBadge color={getBadge(item.status)}>
                                  {item.status}
                                </CBadge>
                              </td>
                            ),
                      }}
                    />
                </CCard>
                </div>
            </CRow>
            {/* <Penumpangs /> */}
          </div>
        )
      }else if(type === 'armada'){
        return(
          <div>
            <CCard className='p-3'>
              <MapLocation id={id} id_keberangkatan={idKeberangkatan}/>
            </CCard>
            <CRow>
                <div className='col-lg-6 col-xs-12 col-sm-12 col-md-6'>
                    <h5 className="heading-text">List Kapal</h5>
                    <div className="card">
                        {kapal.data ? getCardKapal(kapal.data) : 'asdad'}
                    </div>
                </div>
                <div className='col-lg-6 col-xs-12 col-sm-12 col-md-6'>
                    <h5 className="heading-text">List Keberangkatan</h5>
                      <div className='card blue-thead'>
                              <CDataTable
                              items={jadwalnya}
                              fields={[
                                { key: 'nahkoda', _style: { width: '15%'}},
                                { key: 'jadwal', _style: { width: '10%'} },
                                { key: 'nama_kapal', _style: { width: '10%'} },
                                { key: 'tujuan_awal', _style: { width: '20%'} },
                                { key: 'tujuan_akhir', _style: { width: '10%'} },
                                { key: 'status', _style: { width: '10%'} },
                              ]}
                              columnFilter
                              // tableFilter
                              button
                              hover
                              pagination
                              bordered
                              striped
                              size="sm"
                              itemsPerPage={5}
                              scopedSlots = {{
                                  'nahkoda':
                                    (item, index)=>(
                                      <td key={index}>
                                        {item.jadwal_to_nahkoda.nama_nahkoda}
                                      </td>
                                    ),
                                    'jadwal':
                                    (item)=>(
                                      <td>
                                        {item.jadwal}
                                      </td>
                                    ),
                                    'nama_kapal': 
                                    (item)=> (
                                      <td>
                                        {item?.jadwal_to_kapal?.nama_kapal ? item.jadwal_to_kapal.nama_kapal : '-'}
                                      </td>
                                    ),
                                    'tujuan_awal': 
                                    (item)=> (
                                      <td>
                                        {item.jadwal_to_rute.tujuan_awals.nama_dermaga} - {item.jadwal_to_rute.tujuan_awals.lokasi}
                                      </td>
                                    ),
                                    'tujuan_akhir': 
                                    (item)=> (
                                      <td>
                                        {item.jadwal_to_rute.tujuan_akhirs.nama_dermaga} - {item.jadwal_to_rute.tujuan_akhirs.lokasi}
                                      </td>
                                    ),
                                    'status': 
                                    (item)=> (
                                      <td>
                                        <CBadge color={getBadge(item.status)}>
                                          {item.status}
                                        </CBadge>
                                      </td>
                                    ),
                              }}
                            />
                      </div>
                </div>
            </CRow>
          </div>
        )
      }else if(type === 'loket'){
        return(
        <div >
        <h5 className="heading-text">List Keberangkatan</h5>
          <div className='card blue-thead'>
                  <CDataTable
                  items={jadwalnya}
                  fields={[
                    { key: 'nahkoda', _style: { width: '15%'}},
                    { key: 'jadwal', _style: { width: '10%'} },
                    { key: 'nama_kapal', _style: { width: '10%'} },
                    { key: 'tujuan_awal', _style: { width: '20%'} },
                    { key: 'tujuan_akhir', _style: { width: '10%'} },
                    { key: 'status', _style: { width: '10%'} },
                  ]}
                  columnFilter
                  // tableFilter
                  button
                  hover
                  pagination
                  bordered
                  striped
                  size="sm"
                  itemsPerPage={5}
                  scopedSlots = {{
                      'nahkoda':
                        (item,index)=>(
                          <td key={index}>
                            {item.jadwal_to_nahkoda.nama_nahkoda}
                          </td>
                        ),
                        'jadwal':
                        (item)=>(
                          <td>
                            {item.jadwal}
                          </td>
                        ),
                        'nama_kapal': 
                        (item)=> (
                          <td>
                            {item?.jadwal_to_kapal?.nama_kapal ? item.jadwal_to_kapal.nama_kapal : '-'}
                          </td>
                        ),
                        'tujuan_awal': 
                        (item)=> (
                          <td>
                            {item.jadwal_to_rute.tujuan_awals.nama_dermaga} - {item.jadwal_to_rute.tujuan_awals.lokasi}
                          </td>
                        ),
                        'tujuan_akhir': 
                        (item)=> (
                          <td>
                            {item.jadwal_to_rute.tujuan_akhirs.nama_dermaga} - {item.jadwal_to_rute.tujuan_akhirs.lokasi}
                          </td>
                        ),
                        'status': 
                        (item)=> (
                          <td>
                            <CBadge color={getBadge(item.status)}>
                              {item.status}
                            </CBadge>
                          </td>
                        ),
                  }}
                />
          </div>
        </div>
        )
      }
    })()}
    </>
  )
}

export default Dashboard
