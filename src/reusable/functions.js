export function formatToIndonesianCurrency(number) {
    const integerPart = parseInt(number).toLocaleString('id-ID');
    let formattedNumber = "Rp " + integerPart;
  
    return formattedNumber;
}